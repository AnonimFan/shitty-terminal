#include <SDL2/SDL.h>
////#include <SDL2/SDl_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#ifdef _WIN32
#    include <windows.h>

#define sleep(num)                              \
    Sleep(num * 1000)
#elif _UNIX
#    include <unistd.h>
#else
#error define os
#endif

#include <assert.h>

#define STB_IMAGE_IMPLEMENTATION
#include "./stb_image.h"

#include "./la.h"

#define FONT_WIDTH 128
#define FONT_HEIGHT 64
#define FONT_COLS 18
#define FONT_ROWS 7
#define FONT_CHAR_WIDTH (FONT_WIDTH / FONT_COLS)
#define FONT_CHAR_HEIGHT (FONT_HEIGHT / FONT_ROWS)
#define FONT_SCALE 5

// SDL check code
void scc(int code)
{
    if (code < 0) {
        fprintf(stderr, "[ERROR]: %s\n", SDL_GetError());
        exit(1);
    }
}

// SDL check pointer
void *scp(void *ptr)
{
    if (ptr == NULL) {
        fprintf(stderr, "[ERROR]: %s\n", SDL_GetError());
        exit(1);
    }

    return ptr;
}

SDL_Surface *sfc_from_file(const char *path)
{
    int w, h, n;
    unsigned char *pixels = stbi_load(path, &w, &h, &n, STBI_rgb_alpha);

    if(pixels == NULL) {
        fprintf(stderr,
                "[ERROR]: problem loading %s: %s",
                path, stbi_failure_reason());
        exit(1);
    }

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    const Uint32 rmask = 0xff000000;
    const Uint32 gmask = 0x00ff0000;
    const Uint32 bmask = 0x0000ff00;
    const Uint32 amask = 0x000000ff;
#else
    const Uint32 rmask = 0x000000ff;
    const Uint32 gmask = 0x0000ff00;
    const Uint32 bmask = 0x00ff0000;
    const Uint32 amask = 0xff000000;
#endif

    const int depth = 32;
    const int pitch = 4 * w;

    return scp(SDL_CreateRGBSurfaceFrom(
                   (void*)pixels, w, h, depth, pitch,
                   rmask, gmask, bmask, amask));
}

#define ASCII_DISPLAY_LOW 32
#define ASCII_DISPLAY_HIGH 126

typedef struct {
    SDL_Texture *sprsheet;
    SDL_Rect glyph_table[ASCII_DISPLAY_HIGH - ASCII_DISPLAY_LOW + 1];
} Font;

Font fn_load_file(SDL_Renderer *r, const char *path)
{
    Font font = {0};

    SDL_Surface *fn_sfc = sfc_from_file(path);
    font.sprsheet = scp(SDL_CreateTextureFromSurface(r, fn_sfc));
    SDL_FreeSurface(fn_sfc);

    for (size_t ascii = ASCII_DISPLAY_LOW; ascii <= ASCII_DISPLAY_HIGH; ++ascii) {
        const size_t index = ascii - ASCII_DISPLAY_LOW;
        const size_t col = index % FONT_COLS;
        const size_t row = index / FONT_COLS;
        font.glyph_table[index] = (SDL_Rect) {
            .x = col * FONT_CHAR_WIDTH,
            .y = row * FONT_CHAR_HEIGHT,
            .w = FONT_CHAR_WIDTH,
            .h = FONT_CHAR_HEIGHT,
        };
    }

    return font;
}

void render_char(SDL_Renderer *r, Font *font, char c, Vec2f pos, float scale)
{
    const SDL_Rect dst = {
        .x = (int) floorf(pos.x),
        .y = (int) floorf(pos.y),
        .w = (int) floorf(FONT_CHAR_WIDTH * scale),
        .h = (int) floorf(FONT_CHAR_HEIGHT * scale),
    };

    assert(c >= ASCII_DISPLAY_LOW);
    assert(c <= ASCII_DISPLAY_HIGH);
    const size_t index = c - ASCII_DISPLAY_LOW;
    scc(SDL_RenderCopy(r, font->sprsheet, &font->glyph_table[index], &dst));
}

void render_text_sized(SDL_Renderer *r,
                       Font *font,
                       const char *text,
                       size_t text_size,
                       Vec2f pos,
                       Uint32 color,
                       float scale)
{
    scc(SDL_SetTextureColorMod(
            font->sprsheet,
            (color >> (8 * 0)) & 0xff,
            (color >> (8 * 1)) & 0xff,
            (color >> (8 * 2)) & 0xff));

    scc(SDL_SetTextureAlphaMod(font->sprsheet, (color >> (8 * 3)) & 0xff));

    Vec2f pen = pos;
    for (size_t i = 0; i < text_size; ++i) {
        render_char(r, font, text[i], pen, scale);
        pen.x += FONT_CHAR_WIDTH * scale;
    }
}

void render_text(SDL_Renderer *r,
                 Font *font,
                 const char* text,
                 Vec2f pos,
                 Uint32 color,
                 float scale)
{
    render_text_sized(r, font, text, strlen(text), pos, color, scale);
}

#define BUFF_CAP 16777216
char buffer[BUFF_CAP];
size_t buff_cursor = 0;
size_t buff_size = 0;

#define UNHEX(color) \
    ((color) >> (8 * 0)) & 0xff, \
    ((color) >> (8 * 1)) & 0xff, \
    ((color) >> (8 * 2)) & 0xff, \
    ((color) >> (8 * 2)) & 0xff

void render_cursor(SDL_Renderer *r, Uint32 color)
{
    const SDL_Rect rect = {
        .x = (int) floorf(buff_cursor * FONT_CHAR_WIDTH *  FONT_SCALE),
        .y = 0,
        .w = FONT_CHAR_WIDTH * FONT_SCALE,
        .h = FONT_CHAR_HEIGHT * FONT_SCALE,
    };

    scc(SDL_SetRenderDrawColor(r, UNHEX(color)));
    scc(SDL_RenderFillRect(r, &rect));
}

int main()
{
    scc(SDL_Init(SDL_INIT_VIDEO));

    SDL_Window *win =
        scp(SDL_CreateWindow("shitty", 0, 0, 800, 600,
                             SDL_WINDOW_RESIZABLE));

    SDL_Renderer *r =
        scp(SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED));

    Font font = fn_load_file(r, "./charmap-oldschool_white.png");

    bool quit = false;
    while (!quit) {
        SDL_Event event = {0};
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT: {
                quit = true;
            } break;
            case SDL_KEYDOWN: {
                switch (event.key.keysym.sym) {
                case SDLK_BACKSPACE: {
                    if (buff_size > 0) {
                        buff_size -= 1;
                        buff_cursor = buff_size;
                    }
                } break;
                }
            } break;

            case SDL_TEXTINPUT: {
                size_t text_sz = strlen(event.text.text);
                const size_t free_space = BUFF_CAP - buff_size;
                if (text_sz > free_space) {
                    text_sz = free_space;
                }
                memcpy(buffer + buff_size, event.text.text, text_sz);
                buff_size += text_sz;
                buff_cursor = buff_size;
            } break;
            }
        }

        scc(SDL_SetRenderDrawColor(r, 0, 0, 0, 0));
        scc(SDL_RenderClear(r));

        render_text_sized(r,
                          &font,
                          buffer,
                          buff_size,
                          vec2f(0.0, 0.0),
                          0xFFFFFFFF,
                          FONT_SCALE);
        render_cursor(r, 0xFFFFFFFF);

        SDL_RenderPresent(r);
    }

    SDL_DestroyWindow(win);
    SDL_DestroyRenderer(r);
    SDL_Quit();

    return 0;
}

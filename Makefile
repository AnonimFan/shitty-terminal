CC = cc -Wall -Wextra -pedantic -D_UNIX -ggdb -std=c11
LD = $(CC) -o shitty -lm -lSDL2 -lSDL2_image -lSDL2main 

all: comp link

comp:
	$(CC) -c -o main.o main.c
	$(CC) -c -o la.o la.c 

link:
	$(LD) main.o la.c

install:
	cp ./shitty /usr/local/bin

uninstall:
	rm -rf /usr/local/bin/shitty

remove: uninstall

clean: uninstall
	rm -rf ./*.o
	rm -rf ./shitty
